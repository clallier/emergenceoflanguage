#!/bin/bash
clear
echo '
_  _   _  _     ___          _     
| || | | || |   / _ \___   __| |___ 
| || |_| || |_ / /_)/ _ \ / _` / __|
|__   _|__   _/ ___/ (_) | (_| \__ \
   |_|    |_| \/    \___/ \__,_|___/ '
echo ''
echo 'Bienvenue dans 44Pods                    '
echo '*****************************************'
echo '*Tapez votre choix et appuyez sur Entrée*'
echo '*****************************************'
echo '* 1 - Demarrer en Mode Graphique        *'
echo '* 2 - Demarrer en Mode Console          *'
echo '* 3 - Ouvrir la presentation            *'
echo '* 4 - Quitter                           *'
echo '*****************************************'
read -r choix
path=`dirname $0`
if [ "$choix" = "1" ]; then
	java -jar $path/dist/44PodsLittleScreen.jar
	rm $path/out.txt
elif [ "$choix" = "2" ]; then
	java -jar $path/dist/theCakeIsALie.jar
elif [ "$choix" = "3" ]; then
    open -a /Applications/Safari.app $path/presentation.html
else 
	echo 'Au revoir'  
fi