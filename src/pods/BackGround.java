package pods;


import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;

import algoGen.AG;

//******************************************************************************
/** Class Background
 * 
 * Affiche un fond pour les appli
 * 
 */
//******************************************************************************
public class BackGround extends Frame implements Runnable, MouseListener, MouseMotionListener {

    static final long serialVersionUID = 258;
    private Thread renderThread;                                                // Un thread pour le rendu graphique           									// Un autre pour le calcul de l'algo
    private boolean running = false;                                            // Stop le prog
    private boolean isPaused = false;                                           // Pause
    static int fps = 0;        							// stoque le fps
    static int ups = 0;								// Stoque les ups
    private int frames = 0;                                                     // utilisé pour le calcul de fps
    private long totalTime = 0;                                                 // idem
    private long curTime = System.currentTimeMillis();				//idem
    private long lastTime = curTime;  						// idem
    private long period = 50;       						// la periode de rafraichissement du render en ms ( long car il est comparé à d'autres long )
    private BufferStrategy strategy;        					// la strategie de buffering 
    private static int PanelWidth = 1024;      					// La largeur de l'application 
    private int PanelHeight = 768;      					// Et sa hauteur
    private Graphics buffer;                                                    // Buffer d'affichage
    //******************************************************************************
    // Declaration des variables
    private UniverseWindow Universe;                                              // La fenetre Univers
    private LogWindow Log;                                                        // La fentre Log
    private InfoWindow Info;                                                      // La fenetre Info
    private TaskBar Bar;                                                        // La barre
    private GeneticWindow Gen;                                                    // La fenetre Genetic                            
    private boolean LogIsClicked = false;                                       // Utile pour la gestion des evenements souris
    private boolean UniverseIsClicked = false;                                  // idem
    private boolean GenIsClicked = false;                                       // idem
    private boolean InfoIsClicked = false;                                      // idem
    private int nbMouseClick = 0;                                               // idem

    public BackGround() {
        GraphicsEnvironment ge;
        ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.getDefaultScreenDevice();
        
        // On définit la taille de la fenetre
        PanelWidth = 1024;
        PanelHeight = 700;
        
        // On lui donne cette taille
        this.setSize(PanelWidth, PanelHeight);
        // On enlève les décorations de la fenêtre
        setUndecorated(true);
        // On empêche le redimensionnement
        setResizable(false);
        // On rend la fenetre visible
        setVisible(true);
        // On dessine un background noir
        setBackground(new Color(0, 0, 0));

        // Pour le Fullscreen (trop de problèmes)
        //monitor.setFullScreenWindow(this);
        //monitor.setDisplayMode(dm);

        // Créé 2 buffers dans la VRAM
        createBufferStrategy(2);
        
        // Récupère les buffers de la VRAM
        strategy = getBufferStrategy();
        
        // Et Pouf...
        buffer = strategy.getDrawGraphics();
        
        // keyboard events
        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                executeKeyboardCommand(e.getKeyCode());
            }
        });
        
        // On construit la barre et les fenetres
        Bar = new TaskBar("", 0, 0);
        Universe = new UniverseWindow("Universe", 50, 50);
        Gen = new GeneticWindow("Genetic Code", 100, 300);
        Log = new LogWindow("Log", 450, 300);
        Info = new InfoWindow("Informations", 400, 50);

        // On les oblige à avoir un ordre sur le plan d'affichage
        this.add(Universe, 0);
        this.add(Log, 1);
        this.add(Info, 2);
        this.add(Gen, 3);

        // On ajoute les "ecouteurs d'evenements clavier et souris
        addMouseListener(this);
        addMouseMotionListener(this);
	
        
        // On affiche dans la fenetre de Log le bon deroulement des opérations
        LogWindow.setNewInformation("Windows created");
        LogWindow.setNewInformation("Universe initialisated");
        LogWindow.setNewInformation("InfoPanel initialisated");
        LogWindow.setNewInformation("Genetic viewer initialised");
        LogWindow.setNewInformation("Carrefuly the cake is a lie");

        // On démarre le Thread
        renderThread = new Thread(this);    										// crée le thread 
        renderThread.start();               										// Et le demarre
    }
    //*****************************************************************************
    /* Gestion evenements clavier */

    private void executeKeyboardCommand(int keyCode) {
        if (keyCode == KeyEvent.VK_ESCAPE) {										// Echappe : arrete le programme
            //System.out.println("KeyCode"+keyCode);
            running = false;
        }
        if (keyCode == KeyEvent.VK_F2) {    										// F2 pause/reprend le thread
            if (isPaused == true) {
                isPaused = false;
            } else {
                isPaused = true;
            }
        }
        if (keyCode == KeyEvent.VK_SPACE) {
            Log.clearTheLogPanel();
        }
        if (keyCode == KeyEvent.VK_Z) {
            AG.setPeriod("20");
            setPeriod(50);
            LogWindow.setNewInformation("Computation time is set to normal mode");
        }
        if (keyCode == KeyEvent.VK_A) {
            AG.setPeriod("1000");
            LogWindow.setNewInformation("Computation time is set to slow mode");
        }
        if (keyCode == KeyEvent.VK_E) {
            AG.setPeriod("1");
            setPeriod(1000);
            LogWindow.setNewInformation("Computation time is set to hell mode");
        }
        if (keyCode == KeyEvent.VK_H) {
            Info.setIsMinimised();
            Log.setIsMinimised();
            Universe.setIsMinimised();
            Gen.setIsMinimised();
            LogWindow.setNewInformation("show hide show hide show hide show");
        }

    }
//******************************************************************************
    /* le Main Loop */
    // Run est implémenté par l'interface Runnable
    //@Override ne fonctionne qu'avec java 1.6
    public void run() {
        running = true;                     									// Change l'état du systeme
        long beforeTime;                    									// Stoque le nb de ms de ref
        long timeDiff;                      									// Stock le nb de ms depuis l'entrée dans la boucle (while)
        long sleepTime;                     									// Stock le temps restant à "dormir" avant la fin des 20ms
        beforeTime = System.currentTimeMillis();
        while (running) {                    									// Tant que l'utilisateur ne break pas la boucle

            try {
                gameRender();
                gameUpdate();
                timeDiff = System.currentTimeMillis() - beforeTime;
                sleepTime = period - timeDiff; 									// Temps restant dans le loop
                storeStatsRender();                								// Met les stats du jeu a jour
                Thread.sleep(sleepTime);
            } catch (Exception e) {
            }
            beforeTime = System.currentTimeMillis();
        }
        System.exit(0);                 										// Ferme le programme
    }
//******************************************************************************
    //update le jeu d'apres les evenements
    private void gameUpdate() {
        if (!isPaused) {                     									// Si le jeu n'est pas stoppé ni en pause
            getLife();                      									// Compute les translation d'items
            transmitToUniverse(); // Transmission des infos à la fenetre Universe
        }
    }

//******************************************************************************
    // Dessine/Ecrit sur le backbuffer
    private void gameRender() {
        // Burn!! Si fond coloré utiliser fillRect
        buffer.setColor(Color.black);
        buffer.fillRect(0, 0, PanelWidth, PanelWidth);
        //buffer.clearRect(0, 0, PanelWidth, PanelWidth);
        Bar.paint(buffer);
        buffer.setColor(new Color( 195, 5, 25));
        //Graph.paint(buffer);
        Universe.paint(buffer);
        Log.paint(buffer);
        Info.paint(buffer);
	Gen.paint(buffer);

        
        // On affiche la fenetre clickée au premier plan
        if (getComponentZOrder(Universe) == 0) {
            Universe.paint(buffer);
        }
        if (getComponentZOrder(Log) == 0) {
            Log.paint(buffer);
        }
        if (getComponentZOrder(Info) == 0) {
            Info.paint(buffer);
        }
		if (getComponentZOrder(Gen) == 0) {
            Gen.paint(buffer);
        }
        
        
        // On affiche les commandes disponibles dans la barre
        showCommandKey(buffer);
        // on envoie toutes les données du buffer mémoire vers le buffer d'affichage
        strategy.show();

    }
//******************************************************************************
    // Met à jour les stats
    public void storeStatsRender() {
        lastTime = curTime;
        curTime = System.currentTimeMillis();
        totalTime += curTime - lastTime;
        if (totalTime > 1000) {
            totalTime -= 1000;
            fps = frames;
            frames = 0;
        }
        frames++;
    }
//******************************************************************************
    // getLife()
    public void getLife() {
        for (int i = 0; i < 4; i++) {
            Info.animAll();
            Log.animAll();
            Universe.animAll();
            Bar.animAll();
			Gen.animAll();
        }
    }
//******************************************************************************
//******************************************************************************
    // Transmet les infos à la frenetre Universe
    public void transmitToUniverse() {
        Universe.razIndInList();
        for (int i = 0; i<AG.getPopSize(); i++){
            Universe.setIndInList(AG.getId(i).getX(), AG.getId(i).getY(), AG.getId(i).getSex());
        }
    }
//******************************************************************************  
    // Retourne les fps
    public static int getFps() {
        return fps;
    }
//******************************************************************************
    // Gestion des evenements souris
    public void mouseMoved(MouseEvent e) {
    }
    // Gestion du deplacement des fenetres
    public void mouseDragged(MouseEvent e) {
        if (LogIsClicked) {
            if (getComponentZOrder(Log) == 0) {
                    Log.setLocation(e.getX(), e.getY());
            }
        }
        if (UniverseIsClicked) {
            if (getComponentZOrder(Universe) == 0) {
                    Universe.setLocation(e.getX(), e.getY());
            }
        }
        if (InfoIsClicked) {
            if (getComponentZOrder(Info) == 0) {
                    Info.setLocation(e.getX(), e.getY());
            }
        }
		if (GenIsClicked) {
            if (getComponentZOrder(Gen) == 0) {
                    Gen.setLocation(e.getX(), e.getY());
            }
        }
       // System.out.println("MOUSEX" + e.getX());
    }
    
    // Gestion du double click pour la minimisation
    public void mouseClicked(MouseEvent e) {
        if (nbMouseClick == 1) {
            if (Universe.isClicked(e.getX(), e.getY())) {
               if (getComponentZOrder(Universe) == 0) {
                    Universe.setIsMinimised();
                }
            }
            if (Info.isClicked(e.getX(), e.getY())) {
				if (getComponentZOrder(Info) == 0) {
                    Info.setIsMinimised();
                }
            }
            if (Log.isClicked(e.getX(), e.getY())){
                if (getComponentZOrder(Log) == 0) {
                    Log.setIsMinimised();
                }
            }
			if (Gen.isClicked(e.getX(), e.getY())){
                if (getComponentZOrder(Gen) == 0) {
                    Gen.setIsMinimised();
                }
            }
            nbMouseClick = 0;
        } else {
            nbMouseClick++;
        }
    }
    
    //Invoked when the mouse has been clicked on a component.
    public void mousePressed(MouseEvent e) {
        //isClicked = true;
        if (Universe.isClicked(e.getX(), e.getY())) {
            this.setComponentZOrder(Universe, 0);
            System.out.println("UNIVERSE" + getComponentZOrder(Universe));
            UniverseIsClicked = true;
        }
        if (Log.isClicked(e.getX(), e.getY())) {
            this.setComponentZOrder(Log, 0);
            System.out.println("LOG" + getComponentZOrder(Log));
            LogIsClicked = true;
        }
        if (Info.isClicked(e.getX(), e.getY())) {
            this.setComponentZOrder(Info, 0);
            System.out.println("INFO" + getComponentZOrder(Info));
            InfoIsClicked = true;
        }
	if (Gen.isClicked(e.getX(), e.getY())) {
	    this.setComponentZOrder(Gen, 0);
	    System.out.println("Gen" + getComponentZOrder(Info));
	    GenIsClicked = true;
	}

    }
    
    // Quand le bouton de la souris est relaché
    public void mouseReleased(MouseEvent e) {
        LogIsClicked = false;
        UniverseIsClicked = false;
        InfoIsClicked = false;
	GenIsClicked = false;
		
    }
	
    //Invoked when a mouse button has been released on a component.
    public void mouseEntered(MouseEvent e) {
    }
    //Invoked when the mouse enters a component.
    public void mouseExited(MouseEvent e) {
    }
    //Invoked when the mouse exits a component.
	
//******************************************************************************
    public void showCommandKey(Graphics g){
        g.setColor(new Color( 250, 100, 60));
        g.setFont(new Font("Arial",Font.BOLD,12));
        g.drawString("| ESC - Quit | Z - Set normal compuation time | A - Set slow computation time | E - Set boost computation time | H - Hide or Show All Window |", 10, 15);
    }
    public static int getPanelWidth(){
		return PanelWidth;
	}
    public void setPeriod(int temp){
        period = temp;
    }
//******************************************************************************   
}
