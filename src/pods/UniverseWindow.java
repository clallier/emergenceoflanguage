
package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import algoGen.AG;


@SuppressWarnings("serial")
public class UniverseWindow extends Window{
    private static ArrayList<int[]> indList;
    private static int PopSize=50;
	private static boolean isClicked = false;
	private int x;
	private int y;
	private boolean isMinimised = false;
    public UniverseWindow(String ID,int  posX, int posY){
        super(ID,posX,posY);
        x = posX;
		y = posY;
        indList = new ArrayList<int[]>();
        setName(ID);
        setLocation(x, y);
        setSize(300, 240);
        //couleur
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 250, 100, 60);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }

    @Override
    public void paint(Graphics g) {

        // Fast acces
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        Color F     = getForeground();
        Font font   = getFont();
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        
        super.paint(g);                             // dessin de la fenetre
        if (!isMinimised){
            W=240;
            H=230;
            setSize(W, H);		
            g.setColor(F);
            g.setFont(font); 
            drawObstacles(g,X,Y,W,H);
            drawIndividus(g,X,Y,W,H);                   // dessin des individus
	}
        else { 
            setSize(100, 20);
            g.setColor(Color.black);
	}
        
    }
    
    
    // On entre la position et le sex de chaque individu dans une liste de tableau
    public void setIndInList(int x, int y, int sex){
        int[] posTable = new int[3];
        posTable[0] = x;
        posTable[1] = y;
        posTable[2] = sex;
        indList.add(posTable);
    }
    
    // Puis a chaque update on dessine tous les individus présent dans cette liste
    public void drawIndividus(Graphics g, int x, int y, int w, int h){
        int[] temptable = new int[4];
        if (!indList.isEmpty()){
            for (int i=0; i<indList.size(); i++){
                temptable = indList.get(i);
                if (temptable[2]==2){
                    g.setColor(new Color(  250, 100, 60));
                }
                 else {
                    g.setColor(Color.gray);
                }
                g.drawRect(x+15+temptable[0]*2, y+15+temptable[1]*2, 2, 2);
            }
        }
    }
    
    // On dessine aussi les obstacles sur la carte
    public void drawObstacles(Graphics g,int x, int y,int w, int h){
        for (int i=0; i<AG.getNumberOfObstacles(); i++){
            g.setColor(Color.black);
            g.drawRect(x+15+AG.getObstaclesX(i)*2, y+15+AG.getObstaclesY(i)*2,2,2);
        }
    }
    
    // Pour effacer la liste
    public void razIndInList(){
        indList.clear();
    }
    
    // Si la Liste est pleine
    public boolean isCompleted(){
        if (indList.size()>AG.getPopSize()){
            return true;
        }
        else {
            return false;
        }
    }
   
    // Recuperer la taille de la liste utile pour le debug
    public static int getListSize(){
	return indList.size();
    }
	
    public void setIsMinimised(){
	if (!isMinimised){
            isMinimised = true;
            LogWindow.setNewInformation("Universe window is minimised");
	}
	else {
            isMinimised = false;
            LogWindow.setNewInformation("Universe window is deminimised");
	}
    }
    public boolean getIsMinimised(){
	return isMinimised;
    }

	public static int getPopSize() {
		return PopSize;
	}

	public static void setPopSize(int popSize) {
		PopSize = popSize;
	}

	public static boolean isClicked() {
		return isClicked;
	}

	public static void setClicked(boolean isClicked) {
		UniverseWindow.isClicked = isClicked;
	}
}
