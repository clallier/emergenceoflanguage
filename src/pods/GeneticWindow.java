package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import algoGen.AG;


@SuppressWarnings("serial")
public class GeneticWindow extends Window {
    // variable pour savoir si la fenetre est minimisée
    private boolean isMinimised = false;
    // COnstructeur
    public GeneticWindow(String ID,int  posX, int posY){
        super(ID,posX,posY);
	setName(ID);
        setLocation(posX, posY);
        setSize(300, 240);
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 250, 100, 60);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }

   
    @Override
    public void paint(Graphics g) {

        // Fast acces
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        
        // dessin de la Fenetre mére Dows
        super.paint(g);
	
    	// Contenu de la fenêtre @START
        // Lorsque non minimisée
	if (!isMinimised){
            W=300;
            H=300;
            setSize(W, H);
            drawChromosomes(g,X,Y,W,H);
    	}
    	// @END
    	// Lorsque la fenêtre est minimisée @START
	else {
            setSize(100, 20);
	}
    	//@END
    }
	
    public void setIsMinimised(){
        if (!isMinimised){
            isMinimised = true;
            LogWindow.setNewInformation("Genetic window is minimised");
		}
		else {
            isMinimised = false;
            LogWindow.setNewInformation("Genetic window is deminimised");
		}
    }

    public boolean getIsMinimised(){
		return isMinimised;
    }
    // Affichage du code Génétique de 270 individus
    private void drawChromosomes(Graphics g, int X, int Y, int W, int H) {
        Color G = new Color(20,20,20);
        int taille=270;
        if (AG.getPopSize() <= 270){
           taille = AG.getPopSize();
        }
        for (int i=2; i<25; i++){
            for (int j=0; j<taille; j++){
                if (AG.getGene(j,i)==1){
                    G = new Color(175,5,25);
                }
                if (AG.getGene(j,i)==2){
                    G = new Color(155,5,20);
                }
                if (AG.getGene(j,i)==3){
                    G = new Color(135,0,20);
                }
                if (AG.getGene(j,i)==4){
                    G = new Color(115,0,20);
                }
                if (AG.getGene(j,i)==5){
                    G = new Color(105,0,15);
                }
                if (AG.getGene(j,i)==6){
                    G = new Color(95,0,15);
                }
                if (AG.getGene(j,i)==7){
                    G = new Color(75,0,10);
                }
                if (AG.getGene(j,i)==8){
                    G = new Color(55,0,10);
                }
                g.setColor(G);
               // g.drawFillRect(X+10+(i*50),Y+20+(j*6), 4, 4);
                g.fillRect(X+15+(i*10),Y+20+(j*1), 10, 1);
            }
            
        }
    }
}
    
