
package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import algoGen.*;

@SuppressWarnings("serial")
public class InfoWindow extends Window {
    // variable pour savoir si la fenetre est minimisée
    private boolean isMinimised = false;
    // COnstructeur
    public InfoWindow(String ID,int  posX, int posY){
        super(ID,posX,posY);
	setName(ID);
        setLocation(posX, posY);
        setSize(300, 240);
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 250, 100, 60);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }

   // @Override
    @Override
    public void paint(Graphics g) {

        // Fast acces
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        Color F     = getForeground();
        
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        
        // dessin de la Fenetre mère Dows
        super.paint(g);
	
        // Contenu de la fenêtre @START
	if (!isMinimised){
            W = 300;
            H = 240;
            setSize(W,H);
            g.setColor(F);
            g.setFont(new Font("Arial",Font.PLAIN,12));
            g.drawString("FPS: "+BackGround.fps, X+10, Y+30);
            g.drawString("UPS: "+AG.getUps(), X+10, Y+45);
            
            g.setFont(new Font("Arial",Font.BOLD,12));
            g.drawString("GENERAL INFORMATION",X+10,Y+75);
            g.setFont(new Font("Arial",Font.PLAIN,12));
            g.drawString("Pop. Totale:"+AG.getPopSize(),X+10,Y+90);
            g.drawString("Nb Cycles: "+AG.getCycle(), X+120, Y+90);
            g.drawString("Total Morts: "+AG.getTotDeath(),X+10,Y+105);
            g.drawString("Total Reproduction: "+AG.getTotChilds(),X+10,Y+120);
            
            g.setFont(new Font("Arial",Font.BOLD,12));
            g.drawString("TURN INFORMATION",X+10,Y+150);
            g.setFont(new Font("Arial",Font.PLAIN,12));
            g.drawString("LoveMessage :"+AG.getThisTurnLoveMess(),X+10,Y+165);
            g.drawString("Nb Reproduction : "+AG.getReproThisTurn(),X+10,Y+180);
            g.drawString("Nb Morts : "+AG.getDeathThisTurn(),X+10,Y+195);
            g.drawString("Age Moyen : "+AG.getAverageAge(),X+10,Y+210);
            g.drawString("Ratio H/F : "+AG.getRatioHF(),X+10,Y+225);
			
			
        }
        // @END
        // Lorsque la fenêtre est minimisée @START
	else {
            setSize(100, 20);
	}
        //@END
    }
	
    public void setIsMinimised(){
        if (!isMinimised){
            isMinimised = true;
            LogWindow.setNewInformation("Info window is minimised");
	}
	else {
            isMinimised = false;
            LogWindow.setNewInformation("Info window is deminimised");
	}
    }
    public boolean getIsMinimised(){
	return isMinimised;
    }
}
    
