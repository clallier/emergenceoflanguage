
package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class LogWindow extends Window {
    
    // Pour savoir si la fenetre est minimisée
    private boolean isMinimised = false;
    // La liste qui contient les phrases de Log
    private static ArrayList<String> ContentList;
    // Le nombres d'info maximum affichées par la fenetre
    private final static int maximumInfo=20;
    
    // Constructeur
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public LogWindow(String ID,int  posX, int posY){
        super(ID,posX,posY);
        ContentList = new ArrayList();
        setName(ID);
        setLocation(posX, posY);
        setSize(300, 240);
        //couleur
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 250, 100, 60);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }

    @Override
    public void paint(Graphics g) {

        // Fast acces
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        Color F     = getForeground();
        Font font   = getFont();
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        
        g.setFont(font);
        super.paint(g);
        g.setFont(new Font("Courrier",Font.PLAIN,10));
        g.setColor(F);
		
	if (!isMinimised){
            W = 300;
            H = 240;
            setSize(W,H);
            drawInformations(g,X,Y);
	}
	else {
            setSize(100, 20);
	}
        
    }
    
    // Ajouter une nouvelle info dans la liste 
    public static void setNewInformation(String info){
        if (ContentList.size()==maximumInfo){
            ContentList.remove(0);
        }
        ContentList.add("PodsOS.core%-> "+info);
    }
    
    // Afficher les infos qui ont été ajoutées dans la liste
    public void drawInformations(Graphics g, int x, int y){
        int positionY = 40;
        for (int i=0; i<ContentList.size(); i++){
            g.drawString(ContentList.get(i),x+10,y+positionY);
            positionY += 10;
        }
    }
    
    
    // Effacer les infos affichées dans la fenetre
    public void clearTheLogPanel(){
        ContentList.clear();
    }
    
    // Minimisation de la fenetre
    public void setIsMinimised(){
	if (!isMinimised){
            isMinimised = true;
            LogWindow.setNewInformation("Log window is minimised");
	}
	else {
            isMinimised = false;
            LogWindow.setNewInformation("Log window is deminimised");
	}
    }
    public boolean getIsMinimised(){
	return isMinimised;
    }
	
}
