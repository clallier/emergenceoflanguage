
// Affiche la barre
// herite de Dows comme les fenetres
package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class TaskBar extends Item{
	public TaskBar(String ID, int posX, int posY) {
		
	setName(ID);
        setLocation(posX, posY);
        setSize(BackGround.getPanelWidth(), 25);
        //couleur
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 175, 60, 40);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }
	
    @Override
    public void paint(Graphics g) {
		
        // Fast acces
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        Color B     = getBackground();
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        g.setColor(B);
        
        g.fill3DRect(X,Y,W,H,true);                        // Barre
		
        
        g.setColor(B.darker());                             // La meme couleur plus foncé
        g.drawRect(X, Y, W, H);                             // Tour de la barre
	g.drawRect(X+1,Y+1,W-1,H-1);                        // Tour de la barre
    }
	
	
}
