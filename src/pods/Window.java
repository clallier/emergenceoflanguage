
package pods;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;


/*
 Dows est une fenetre prototypique, les autres fenetres herite de ces propriètés
 
 */
@SuppressWarnings("serial")
public class Window extends Item{
        // Constructeur
	public Window(String ID, int posX, int posY) {
        // On enregistre le nom donné à la fenetre
	setName(ID);
        // On lui donne une position
        setLocation(posX, posY);
        // et une taille
        setSize(300, 240);

        //couleur
        Color b = new Color( 195, 5, 25);
        Color f = new Color( 175, 60, 40);
        Font font = new Font("Arial",Font.PLAIN,12);
        setBackground(b);
        setForeground(f);
        setFont(font);
    }

    @SuppressWarnings("unused")
	@Override
    public void paint(Graphics g) {

        // Fast acces
        String ID   = getName();
        int X       = getX();
        int Y       = getY();
        int W       = getWidth();
        int H       = getHeight();
        Color F     = getForeground();
        Color B     = getBackground();
        Font font   = getFont();
        //System.out.println(ID+" x:"+X+" y:"+Y+" w:"+W+" h:"+H);
        g.setColor(B);
        
        g.fillRect(X,Y,W,H);                        // Fenetre Principale
        
        g.setColor(B.darker());                     // Tour  foncé de la Fenetre
        g.drawRect(X, Y, W, H);
        
        g.fillRect(X+W+1,Y+4,3,H);                  // Ombre Verticale
        g.fillRect(X+5,Y+H+1,W-4,3);                // Ombre Horizontale
        
        g.setColor(F);
        g.fill3DRect(X+W-17, Y+7, 10, 10, false);   // Bouton 
        g.fill3DRect(X+W-13, Y+3, 10, 10, true);    // Bouton 
        
        g.setFont(new Font("Arial",Font.BOLD,12));  // Le titre de la fenetre est en Gras
        g.drawString(ID, X+5, Y+15);                // Titre de la Fenetre
    }
	
}
