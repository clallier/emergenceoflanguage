
package pods;


import java.awt.Container;
import java.awt.Graphics;


/**
 * * Item * : représentant graphique d'un objet quelquonque
 * doit servir de base à * Fenetre * et a tt autre truc
 * ex : txt, graphique, point, croix (pour fermer)
 * constitué d'une position x,y,z identifiant
 * peut : 
 * 0) posseder des objets (dont des Items)
 * 1) étre déplacé ( cf slideTo )
 * 2) changer de z index
 * 3) dessiner un truc 
 * 4) ?? resize (peut etre utile pour fenetre)
 * 5) supprimer
 * 6) avoir une couleur
 */

/**
 * herite de Container
 *
 * components methods : 
 *   get/setName()
 *   get/setFont()
 *   get/setSize()
 *   get/setCursor()
 *   get/setForGround()		=>color
 *   get/setBackGround()	=>color
 *   is/setVisible()
 *   setLocation()
 *   getWidth()
 *   getHeight()
 *   getX()
 *   getY()
 *   getMousePosition() 
 *   getGraphics()
 *   *paint()
 *   paintAll()
 *   *toString()
 *   *list()
 *   ...
 *
 *Container methods : 
 *    getComponent(int n)	// renvoie le component n
 *    getComponents() 		// renvoie tt les components contenus
 *    add(Component comp) 	// ajoute un component
 *    addDelicately() 
 *    remove()
 *    removeDelicately()
 *    removeAll()
 *    get/setComponentZOrder(Component comp, int index)
 *    get/findComponentAt()	// picking!
 *    paint()
 *    paintComponents()
 *    toString()
 *    list()
 *    ...
 */


//public abstract class Item extends Component {
@SuppressWarnings("serial")
public abstract class Item extends Container { 
    private int targetX;        // Position finale en x apres translation
    private int targetY;        // Idem en y
	private int posX;        // Position finale en x apres translation
    private int posY;        // Idem en y
    private float stepX;        // Incrémenteur de la translation en X
    private float stepY;        // Idem en Y
    private float time = 0;     // Temps restant pour la translation
    private int slideSpeed = 30; // Durée en frame (fps) de la translation
	private float SLIDESPEED 		= 	33;
	private final int ROTATESPEED 	= 	10;
	private final int DECALX		=	14;
	private final int DECALY		=	44;

//******************************************************************************
    /* Constructeur par defaut */
    public Item() {}
//******************************************************************************
    /* paint method */
    @Override
	public abstract void paint(Graphics g);
//******************************************************************************    
    /* slideSpeed */
    public void setSlideSpeed(int slideSpeed) {
        this.slideSpeed = slideSpeed;
    }
    public int getSlideSpeed() {
        return this.slideSpeed;
    }
//******************************************************************************
    /* Translation */
	public void slideTo(int x, int y){
		//sliding = true;
		x -= DECALX;
		y -= DECALY;
		this.setTargetX(x);
		this.setTargetY(y);
		stepX = (( x - getX() )/SLIDESPEED);
		stepY = (( y - getY() )/SLIDESPEED);
		time = SLIDESPEED;
		//System.out.println("stepX and stepY changed : "+stepX+","+stepY);
	}
	
	
//******************************************************************************
    /** Transformations appliquées à la translation
     *
     * @param t
     * @return
     */
    // Ralenti le slide de façon linéaire
    public float EaseInQuad(float t) {
        //OK uniq pb avec t TODO
        //float x = SLIDESPEED - t;
        //double a = -(x/((SLIDESPEED-1)/2))+(2);
        //System.out.println(x+"speed="+a);
        //return (float)a;
        return 1;
    }
    // Fait "élastiquer" le slide 
    public double EaseOutElastic(float t) {
        /*
         *un truc du style a * exp(-b * x ) + sin ( x * c ) + d;
         */
        return 1.0;
    }
//******************************************************************************
    public void animAll() {
        if (time > 0) {
            int x = getX();
            int y = getY();
            //double speed = EaseOutElastic(time);
            double speed = EaseInQuad(time);
            //double speed =1;
            x += stepX * speed;
            y += stepY * speed;
            setLocation(x, y);
            time--;
        }
    }
//******************************************************************************
    /* click */
    
    public boolean isClicked(int x, int y) {
        if ( (x > getX() ) && ( x < getX() + getWidth()) && ( y > getY() ) && ( y < getY() + getHeight()) ){
                return true;
		}else{
                return false;
        }
    }
	public int getTargetX() {
		return targetX;
	}
	public void setTargetX(int targetX) {
		this.targetX = targetX;
	}
	public int getTargetY() {
		return targetY;
	}
	public void setTargetY(int targetY) {
		this.targetY = targetY;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public int getROTATESPEED() {
		return ROTATESPEED;
	}
}
//******************************************************************************

