
package algoGen;

import java.util.ArrayList;

/**
 *
 * Création d'une population de base
 *
 */

public class Population {
    private ArrayList<Individu> myPop;
    //stats
    int loveMessages = 0;
    int numberOfReproductions = 0;
    int numberOfDeath = 0;
    int TotalDeath = 0;
    int totalRepro = 0;
    float averageAge = 0;
    float ratioHF = 0;
    //debugMode
    private boolean isDebugMode;

    public Population() {   
        isDebugMode = AG.isDebugModeEnable();
        myPop = new ArrayList<Individu>();
        byte p = AG.getmessOffset();

        int genoSize =  p*p + (p+1)*(p+1);
        AG.setChromoSize(genoSize);
        if(isDebugMode){
            System.out.println("message offset : " + p);
            System.out.println("genoSize : " + genoSize);
        }
        for (int i = 0; i < AG.getPopSize(); i++) {
            myPop.add( new Individu(genoSize) );
        }
    }
//******************************************************************************
  public void Evolve(int type){
      switch (type) {
              case 1 : Evolve1(); break;
              case 2 : Evolve2(); break;
      }
  }  
//
    //Evolve de type 1
    public void Evolve1() {
        // distance de propogation du message
        byte p = AG.getmessOffset();
        int l = 2 * p + 1;
        // Un cycle d'evolution : une action faite par tout les individus
        for (int i = 0; i < myPop.size(); i++) {
            if(isDebugMode)
            System.out.println("id:" + i);
            Individu id = myPop.get(i);
            // si id est une fille
            if (id.getSex() == 3 ) {
                if(isDebugMode)
                System.out.println("je suis une fille");
                //augmente le ratio de femmes
                ratioHF++;
                // Elle envoie un message autour d'elle, si un male est dans la zone de message
                for(int j=0; j<myPop.size(); j++){
                    //System.out.println("j:" +j);
                    Individu cible = myPop.get(j);

                    if ( cible.getSex() == 2 ){
                        // coordonnées de id femelle
                        int ix = id.getX();
                        int iy = id.getY();
                        // coordonnées du male cible
                        int cx = cible.getX();
                        int cy = cible.getY();
                        // calcul de la distance  entre id et cible
                        int dx = ix - cx;
                        int dy = iy - cy;
                        if ( cx > ix )
                            dx = cx - ix;
                        if ( cy > iy )
                            dy = cy - iy;

                        if ( (dx + dy) <= p  ){
                            // alors envoie du message
                            // Rappel : 
                            // p = AG.getmessOffset() => puissance maxi du message (sert à determiner la longueur maxi du genome feminin)
                            // l = 2p+1 => "rayon" max du message
                            // dx = abcsisse courante du mess
                            // dy = ordonnee courante du mess
                            // +10 car les 10 premiers bytes du genomes ne comptent pas
                            // Ce sont les marqueur du sexe, de l'age, et du comportement mâle
                                // nb de lignes precedentes         :  p - dy
                                // taille d' une ligne              :  -2*|dy|+l
                                // nb de cases sur la derniere ligne:  p+1- |dy| + dx 
                            int messType = 0;  
                            int nline = (p-dx);
                            for(int k=1; k <= nline ; k++){
                                messType += -2*Math.abs(dy)+l;
                            }
                            messType += p+1- Math.abs(dy) + dx;
                            //System.out.println("gene n° " + messType);
                            messType = id.getGene(messType);
                            id.sendMessage(cible, messType );
                            loveMessages++;
                        } 
                    }
                }
            } else { /* si individu est un garçon */
                if(isDebugMode)
                System.out.println("je suis un garçon");
                int dx = id.getX();
                int dy = id.getY();
                // regarder dans le buffer des messages
                byte mess = (byte) (id.getMessType());
                // Et interpreter le message en mouvement
                // 0 = pas de message => choix de direction aleatoire
                if(mess==0){
                    id.move( AG.getAByte(1, 8) );
                }else{
                    mess++; // decalage , les genes males vont de [2 à 9], alors que les messages vont de [1 à 8] 
                    id.move(id.getGene(mess));// Bouge l'individu male
                }
                //test de repro
                for(int j=0; j< myPop.size(); j++){
                    Individu cible = myPop.get(j);
                    // Boucle sur les individus. Si c'est une fille...
                    if ( cible.getSex() == 3){
                        // alors test de ses coordonnées pour savoir si elle est à coté (N,S,E,W) de id
                        int cx = cible.getX();
                        int cy = cible.getY();
                        boolean repro = false;
                        if(cy == dy)   //si mm ordonnée
                            if( (cx==dx-1) || (cx==dx+1) )
                                repro = true;
                        if(cx == dx)    //si mm abscisse    
                            if( (cy==dy-1) || (cy==dy+1) )
                                repro = true;
                        //si tt est bon => repro
                        // pose du nouveau né aléatoirement
                        // pose du male un peu plus loin aussi
                        if ( repro ){
                            if(isDebugMode)
                            System.out.println("Je suis avec une fille!");
                            crossOver( id, cible);
                            if(isDebugMode)
                            System.out.println("On a fait des enfants!");
                            
                        }
                    }
                }
            }   //fin du test pour males
            if(isDebugMode)
            System.out.println("J'ai "+(id.getAge()+128)+"ans /"+(id.getMaxAge()+128));
            if(id.getAge() >= id.getMaxAge() ){
                myPop.remove(i);
                AG.updatePopSize(-1);
                numberOfDeath ++;
                //System.out.println("Je meurs");
            }
            //age++
            id.updateAge(1);
            averageAge += id.getAge();
        }// fin de la boucle sur tous les individus
    
        //calcule les stats
        computeStatistics();
    }
    
    public Individu getIndividu (int i){
        return myPop.get(i);
    }

    /** Met a jour les statistiques pour chaques generations
     * Calcul des stats globales du programme :
     *  truc a stoquer :
     *  nb de messages d'amour envoyés
     *  nb de reprodution ce tour
     *  nb de mort (pas besoin on a le chgt de pop en tm reel)
     *  age moyen
     *  filles / garçons
     */    
    public void computeStatistics() {
        // calcul des moyennes et ratio;
        averageAge = (averageAge / myPop.size())+128;  
        ratioHF = ratioHF /myPop.size();
        TotalDeath+=numberOfDeath;
        totalRepro+=numberOfReproductions;
        System.out.println("Pop.totale:"+ myPop.size()+"("+AG.getPopSize()+")"+ " Total de morts:"+ TotalDeath+ " Total de reproduction:"+ totalRepro);
        AG.seTotDeath(TotalDeath);
        AG.setTotChilds(totalRepro);
        System.out.println("(Ce tour) : LoveMessages:"+loveMessages+" Repro:"+ numberOfReproductions+" Death:"+ numberOfDeath);
        AG.setThisTurnLoveMess(loveMessages);
        AG.setReproThisTurn(numberOfReproductions);
        AG.setDeathThisTurn(numberOfDeath);
        System.out.println("Age moyen:"+ averageAge+ " Ratio femmes/hommes:" +ratioHF);
        AG.setAverageAge(averageAge);
        AG.setRatioHF(ratioHF);
        
        //reinit pour le prochain cycle
        loveMessages = 0;
        numberOfReproductions = 0;
        numberOfDeath =0;
        averageAge = 0;
        ratioHF = 0;
    }

    private void crossOver(Individu id, Individu cible) {
        byte cross = 0;                     // Croisement sur premier chromosome (= pas de croisement)
        if (AG.isCrossingOver()) // Et si crossOver
        {
            cross = AG.getAByte(AG.getChromoSize());
        }// alors choix du site aléatoirement
        if( myPop.size() <= 2*AG.getInitPopSize() ){
            if(myPop.size() < (AG.getmapH()*AG.getmapW()/4) ){
                int childNumber = AG.getAByte(1, AG.getMaxChildren());
                for(int k=0; k<childNumber; k++){
                    Individu child = new Individu(AG.getChromoSize() );
                    for (int i = 0; i < cross; i++) {
                        child.setGene( i, id.mutation(i) );
                    }
                    for (int i = cross; i < AG.getChromoSize(); i++) {
                        child.setGene(  i, cible.mutation(i) );
                    }
                    myPop.add(child);
                    AG.updatePopSize(1);
                    numberOfReproductions++;
                }
                //apres la boucle de création des enfants, replace le pere aléatoirement
                id.setNewPosition();
            }else{if(isDebugMode)System.out.println("Trop pas de place pour faire des enfants!");}
        }else{if(isDebugMode)System.out.println("Trop de monde!");}

    }

    // evolve de type 2 : LABY
    //calcul la table de selection
    //puis lance la reproduction
    //effectue le calcul des fitness
    //et met à jour les statistiques

     public void Evolve2(){
        // Stock la nouvelle generation
        ArrayList<Individu> newPop = new ArrayList<Individu>();
        //cree la table de selection
        setSelectionTable();
        //position dans newGeneration[]
        int j = 0;
        //remplis la newGeneration[]
        while ( j < myPop.size() ){
            int p1 = selection();  //parent1
            int p2 = selection();  //parent2
            int c1 = j;            //enfant1
            int c2 = j+1;          //enfant2
            //reproduction
            crossOver(myPop.get(p1), myPop.get(p2), newPop.get(c1), newPop.get(c2));
            //calcule le fitness des enfants en fonction de l'evaluation
//            newPop.get(c1).develop();
//            newPop.get(c2).develop();
//            newPop.get(c1).setFitness(newPop.get(c1).fitnessValue());
//            newPop.get(c2).setFitness(newPop.get(c2).fitnessValue());
            j+=2;
        }
        //ecrase l'ancienne generation par la nouvelle
        myPop = newPop;

        //calcule les stats
        computeStatistics();
    }
     
    //construit la table de selection (pour la roulette)
    @SuppressWarnings("unused")
	public void setSelectionTable() {
        float counter = 0f;
        // remplis la table avec n indices de l'individu
        for (int i = 0; i < myPop.size(); i++) {
            //selectionTable[i] = counter;
            counter += myPop.get(i).getFitness();
        }
    }

    //choisi les individus de la table de selection
    public int selection() {
        int j = 0;
        float counter = 0f;
        float roll = AG.getAFloat();
        // remplis la table avec n indices de l'individu
        while( (counter <= roll) && (j < myPop.size() ) ){
            counter += myPop.get(j).getFitness();
            j++;
        }
        return j; //renvoie l'individu selectionne
    }
    
    //reproduction type2 LABY (par crossOver, 2 parents, 2 enfants)
    public void crossOver(Individu parent1, Individu parent2, Individu enfant1, Individu enfant2) {
        //choix de l'emplacement du crossOver
        byte cross = 0;                     // Croisement sur premier chromosome (= pas de croisement)
        if (AG.isCrossingOver()) // Et si crossOver
        {
            cross = AG.getAByte(AG.getChromoSize());
        }// alors choix du site aléatoirement
        //copie de la première partie (parent1 => enfant1, parent2 => enfant2)
        for (int i = 0; i < cross; i++) {
            enfant1.setGene(i, parent1.mutation(i));
            enfant2.setGene(i, parent2.mutation(i));
        }
        //copie de la seconde partie (parent1 => enfant2, parent2 => enfant1)
        for (int i = cross; i < AG.getChromoSize(); i++) {
            enfant2.setGene(i, parent1.mutation(i));
            enfant1.setGene(i, parent2.mutation(i));
        }
    }


}
