
package algoGen;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


public class XMLUtilities {
    private XMLStreamReader reader;
    private XMLStreamWriter writer;
    
    //constructeur
    public XMLUtilities(){
    }
    
    
    
    public void getbyXML( ){
        try {
            File fileToRead = new File("in.xml");
            fileToRead.createNewFile();
            XMLInputFactory factory = XMLInputFactory.newInstance();

            // XMLStreamReader crée un parseur de type Curseur
            reader = factory.createXMLStreamReader(new FileReader(fileToRead));
            while (reader.hasNext()) {
                // Recup du nouvel element
                int type = reader.next();
                switch (type) {
                    case XMLStreamReader.START_ELEMENT:
                        // Si c'est un début de balise user, alors on lance le traitement d'un utilisateur
                        if ("chell.in".equals(reader.getLocalName()))
                        processParsing(reader);
                    break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(XMLUtilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setToXML(){
        try {
            File fileToWrite = new File("out.xml");
            fileToWrite.createNewFile();
            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

            writer = outputFactory.createXMLStreamWriter(new FileWriter(fileToWrite));
            // On ecrit dans le fichier grace aux méthodes writeXXXX
            writer.writeStartDocument(); // donnera <?xml version='1.0' encoding='utf-8'?>
            // Dans l'ordre :
            // Header/flags/truc liés au prog
            writer.writeStartElement("Chell.out");
            writer.writeStartElement("props");
                //debugMode
                writer.writeAttribute("isInDebugMode", ""+AG.isDebugModeEnable());
                //period
                writer.writeAttribute("period", ""+AG.getPeriod());
                //type de terrain => si chgt restart du prog
                writer.writeAttribute("playgroundType", "" + AG.getType());
            writer.writeEndElement();//fin de props
            // Trucs liés a la map
            writer.writeStartElement("map");
                //mapWidth
                writer.writeAttribute("mapWidth", ""+AG.getmapW());
                //mapHeight
                writer.writeAttribute("mapHeight", ""+AG.getmapH());
                //playGround
                    writer.writeStartElement("playGround");
                        
                    writer.writeAttribute("obstacles", ""+AG.getNumberOfObstacles() );
                        writer.writeStartElement("obstactlesList");
                        for (int o = 0; o < AG.getNumberOfObstacles(); o++) {
                                        writer.writeAttribute("obstacleX"+o,""+AG.getObstaclesX(o));
                                        writer.writeAttribute("obstacleY"+o,""+AG.getObstaclesY(o));
                        }
                        writer.writeEndElement();//ferme obstacles list
                    writer.writeEndElement();//ferme playground
                writer.writeEndElement();// ferme map
            
            // truc lies a la pop
            writer.writeStartElement("population");
                //mutationProbability
                writer.writeAttribute("mutationProbability", ""+AG.getMutationProbability());
                //crossOverProbability
                writer.writeAttribute("crossOverProbability", ""+AG.getCrossOverProbability());
                //ageMaximum
                writer.writeAttribute("maxAge", ""+(AG.getMaxAge()+128));
                //chromoSize
                writer.writeAttribute("chromosomeSize", ""+AG.getChromoSize());
                //maxChildren
                writer.writeAttribute("maxChildren", ""+AG.getMaxChildren());
                //initPopulationSize
                writer.writeAttribute("initPopSize", ""+AG.getInitPopSize());
                //maxCycle
                writer.writeAttribute("maxCycle", ""+AG.getMaxCycle());
            writer.writeEndElement();
            
            // truc lies aux stats
            writer.writeStartElement("stats");
                //cycle 
                writer.writeAttribute("cycle", ""+AG.getCycle());
                //Pop.totale
                writer.writeAttribute("popSize", ""+AG.getPopSize());
                //Total de morts
                writer.writeAttribute("totDeath", ""+AG.getTotDeath());
                //Total de reproduction
                writer.writeAttribute("totChilds", ""+AG.getTotChilds());
                //ce tour LoveMessages
                writer.writeAttribute("loveMessOnThisTurn", ""+AG.getThisTurnLoveMess());
                //ce tour Repro
                writer.writeAttribute("reproThisTurn", ""+AG.getReproThisTurn());
                //ce tour Death
                writer.writeAttribute("deathThisTurn", ""+AG.getDeathThisTurn());
                //age moy
                writer.writeAttribute("averageAge", ""+AG.getAverageAge());
                //ratio F/H
                writer.writeAttribute("ratioHF", ""+AG.getRatioHF());
            writer.writeEndElement();  
            
            // Codes genetiques+autres infos (age_sex_pos)
            writer.writeStartElement("individus");
                for(int i=0; i< AG.getPopSize(); i++){
                    writer.writeStartElement("id"+i);
                    //age
                    writer.writeAttribute("age", ""+(AG.getId(i).getAge()+128) );
                    //age maxi
                    writer.writeAttribute("ageMax", ""+(AG.getId(i).getMaxAge()+128));
                    //position x et y
                    writer.writeAttribute("x", ""+AG.getId(i).getX());
                    writer.writeAttribute("y", ""+AG.getId(i).getY());
                    //sex
                    writer.writeAttribute("sex", ""+AG.getId(i).getSex());
                    //fitness
                    writer.writeAttribute("fitness", ""+AG.getId(i).getFitness());
                    //code genetique
                    String genome =  " "+AG.getId(i).getGene(0);
                    for(int k=1; k<AG.getChromoSize(); k++ ){
                        genome += " "+AG.getId(i).getGene(k);
                    }
                    writer.writeAttribute("genome", genome);
                    writer.writeEndElement();
                }
            writer.writeEndElement();
            // Cree un element : méthodes writeStartElement() (et writeEndElement())
       

            writer.writeEndElement();// ferme la balise englobante chell.out
            // Force l'ecriture de la fin du document (et ferme les balises non fermées)
            writer.writeEndDocument();
            writer.close();
//            fileToWrite.setReadable(true);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLUtilities.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }
    
    
    
    // parsing du fichier in.xml
    
private static void processParsing( XMLStreamReader reader) throws XMLStreamException {

    int flag = 0;  
    //period
    //type de terrain => si chgt restart du prog
    //mapWidth
    //mapHeight
    //mutationProbability
    //crossOverProbability
    //ageMaximum
    //popSize
    //maxCycle
    final int FLAG_NONE         = 0;
    final int FLAG_PERIOD       = 1;
    final int FLAG_TYPE         = 2;
    final int FLAG_MAPW         = 3;
    final int FLAG_MAPH         = 4;
    final int FLAG_MUT_PROB     = 5;
    final int FLAG_CROS_PROB    = 6;
    final int FLAG_AGEMAX       = 7;
    final int FLAG_POPSIZE      = 8;
    final int FLAG_MAXCYCLE     = 9;

    

    boolean state = true;
    while (reader.hasNext() && state) {
        int type = reader.next();

        switch (type) {
            // Si c'est un début d'elément, on garde son type
            case XMLStreamReader.START_ELEMENT:
                if (reader.getLocalName().equals("period"))
                    flag = FLAG_PERIOD;
                else if (reader.getLocalName().equals("type"))
                    flag = FLAG_TYPE;
                else if (reader.getLocalName().equals("mapH"))
                    flag = FLAG_MAPH;
                else if (reader.getLocalName().equals("mapW"))
                    flag = FLAG_MAPW;
                else if (reader.getLocalName().equals("mutationProbability"))
                    flag = FLAG_MUT_PROB;
                else if (reader.getLocalName().equals("crossOverProbability"))
                    flag = FLAG_CROS_PROB;
                else if (reader.getLocalName().equals("ageMax"))
                    flag = FLAG_AGEMAX;
                else if (reader.getLocalName().equals("popSize"))
                    flag = FLAG_POPSIZE;
                else if (reader.getLocalName().equals("maxCycle"))
                    flag = FLAG_MAXCYCLE;
                else flag = FLAG_NONE;
                break;

            // Si c'est du texte ou une zone CDATA ...
            case XMLStreamReader.CDATA:
            case XMLStreamReader.CHARACTERS:
                switch (flag) {
                    case FLAG_PERIOD:
                        // et que ce n'est pas une chaîne de blancs
                        if(!(reader.isWhiteSpace()))
                        AG.setPeriod(reader.getText());
                        break;
//                    case FLAG_TYPE:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//                        AG.setType(reader.getText());
//                        break;
//                    case FLAG_MAPH:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//                        AG.setMapH(reader.getText());
//                        break;
//                    case FLAG_MAPW:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//                        AG.setMapW(reader.getText());
//                        break;
//                    case FLAG_MUT_PROB:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
// //                       AG.setMutProb(reader.getText());
//                        break;
//                    case FLAG_CROS_PROB:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//   //                     AG.setCrosProb(reader.getText());
//                        break;
//                    case FLAG_AGEMAX:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//     //                   AG.setAgeMax(reader.getText());
//                        break;
//                    case FLAG_POPSIZE:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//       //                 AG.setPopSize(reader.getText());
//                        break;
//                    case FLAG_MAXCYCLE:
//                        // et que ce n'est pas une chaine de blanc
//                        if(!(reader.isWhiteSpace()))
//         //               AG.setMaxCycle(reader.getText());
//                        break;
                }
                break;
            case XMLStreamReader.END_ELEMENT:
                // Si c'est la fin de la balise user, on change l'indicateur de boucle
                if (reader.getLocalName().equals("chell.in")) state = false;
                break;
        }
    }
}

    
}
