
package algoGen;

import cern.jet.random.engine.MersenneTwister;

public class AG implements Runnable{
    // le rand gen
    private static MersenneTwister twister = new MersenneTwister(new java.util.Date());
    // portée des messages (nb de cases)
    private static byte messageOffset = 3;              //TODO set/get
    // taux de mutation
    private static float mutationProbability = 0.03f;   //TODO set/get
    // taux de crossOver
    private static float crossOverProbability = 0.6f;   //TODO set/get
    // age maxi des individus
    private static int ageMaximum = 70;//max 255
    // taille des chromosomes (!!! surchargée)
    private static int chromoSize = 35;             //TODO set/get
    //nombre maximum d'enfants par couples
    private static byte maxChildren = 1;            // TODO set/get
    // taille de la pop a l'initialisation
    private static int initPopulationSize = 400;
    // taille de la pop courante (limitée à taille à l'initialisation * 2 )
    private static int popSize = initPopulationSize;    // TODO set
    // duree du prog (-1 : infinity)
    private static int maxCycle = -1;
    private static int cycle = 0;
    // le terrain (1 : playground; 2: laby)
    private static int type = 1; // a passer à tt le monde (ou set/get)
    private static int mapWidth = 100;  //max 127
    private static int mapHeight= 100;  //max 127
    private static PlayGround playGround;
    // flag de dessin de la map
    private boolean isMapDrawn = true;
    private boolean isTimePrinted = false;
    private static boolean debugMode = false;  // TODO à passer aux elements (ou get)
    // calcul du sleepTime/ period = 0 : pas de sleepTime
    private static int period = 20; //TODO set/get
    private boolean running = true;
    private long beforeTime;
    private static Population pop;
    //les stats
    private static int loveMessages = 0;
    private static int numberOfReproductions = 0;
    private static int numberOfDeath = 0;
    private static int TotalDeath = 0;
    private static int totalRepro = 0;
    private static float averageAge = 0;
    private static float ratioHF = 0;
    Thread myThread;
    private static int ups = 0;															// Stoque les ups
    private int frames = 0;        												// utilisé pour le calcul de fps
    private long totalTime = 0;        											// idem
    private long curTime = System.currentTimeMillis();							//idem
    private long lastTime = curTime;  
    
    
    public AG(){
        //setUp des données
        myThread = new Thread(this);
        myThread.start();
        restart();
    }
    public void restart(){
        beforeTime = System.currentTimeMillis();
    }
    public void kill(){
        //bob.reinit();
    }
//******************************************************************************
    //run
    //@Override
	public void run(){
        playGround = new PlayGround(mapWidth, mapHeight, 1);
        pop = new Population();
        XMLUtilities XMLer = new XMLUtilities();
        int isItTimeToPrint = 100;
        //LogDows.setNewInformation("Population Initiale Initialisée");
        
        while(running) {
                storeStatsRender();
                
                long a = System.currentTimeMillis();
                //Evolution
                pop.Evolve(1);
                long b = System.currentTimeMillis();
                if(isTimePrinted)
                    System.out.println("computeTime:" + (b - a));
                
                if (isMapDrawn) {
                    playGround.print(pop);
                }
                long c = System.currentTimeMillis();
                if(isTimePrinted)
                    System.out.println("printTime:" + (c - b));
                // Test de validité
                checkForContinues();
                // Sleep
                if (period > 0) {
                    haveALittleSleepTime();
                }
                long d = System.currentTimeMillis();
                if(isTimePrinted)
                   System.out.println("cycleTime:" + (d - a));
                
                // sauvegarde tt ça ds un fichier xml
                if(isItTimeToPrint < 0){
                    isItTimeToPrint = 100;
                    XMLer.setToXML();
                }else{isItTimeToPrint -= (d-a);}
                
                long e = System.currentTimeMillis();
                if(isTimePrinted)
                   System.out.println("ecriture XML:"+ (e - d));
                
                cycle++;
                System.out.println("cycle n:" + cycle);
        }
        
    }
//******************************************************************************
    //regarde si le programme peut continuer
    private void checkForContinues(){
        //si le nb de tour maxi est atteint
        if( (maxCycle != -1) && ((cycle+1) >= maxCycle )){
            running = false;
        }
        //si pop atteint 0
        if(popSize == 0){
            running = false;
        }
    }
 //*****************************************************************************
    private void haveALittleSleepTime(){ 
        try{
            long timeDiff;                      // Stock le nb de ms depuis l'entrée dans la boucle (while)
            long sleepTime;                     // Stock le temps restant à "dormir" avant la fin de la periode
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleepTime = (period - timeDiff); // Temps restant dans le loop
            if (sleepTime <= 0)             // Si il reste du temps avant le prochain loop
                sleepTime = 1;                  // Dort encore! 
            Thread.sleep(sleepTime);        // Dors
        }catch(Exception e){}
            beforeTime = System.currentTimeMillis();
    }
//******************************************************************************
    // Donne un byte aléatoire entre 0 et n1 
    public static byte getAByte(int n1){
        return (byte)(twister.nextFloat()*(n1+1));
    }
    // Donne un byte aléatoire entre n1 et n2 
    public static byte getAByte(int n1, int n2){
        if(n2< n1){
            int stock = n2;
            n2= n1;
            n1= stock;
        }
        n2++;
        if (n1<0)
            n1--;
        int delta = n2-n1;
        return (byte)((twister.nextFloat()*delta)+n1);
    }
    // Donne un float aleatoire enttre ]0f, 1f[
    public static float getAFloat(){
        return twister.nextFloat();
    }
    // Renvoie un booleen selon la probabilité de mutation
    public static boolean isMuting(){
        float test = twister.nextFloat();
        if(test <= mutationProbability){return true;}
        else{return false;}
    }
    // Renvoie un booleen selon la probabilité de cross-over  
    public static boolean isCrossingOver(){
        float test = twister.nextFloat();
        if(test <= crossOverProbability){return true;}
        else{return false;}
    }
//******************************************************************************
    //debug mode
    public static boolean isDebugModeEnable() {
        return debugMode;
                
    }
    public static int getMaxCycle() {
        return maxCycle;
    }
    public static int getCycle() {
        return cycle;
    }
//******************************************
    public static int getPeriod() {
        return period;
    }
    public static void setPeriod(String p) {
        period = Integer.parseInt(p);
    }
//******************************************  
    public static int getType() {
        return type;
    }
    public static void setType(String t) {
        type = Integer.parseInt(t);
        //restart();
    }
//******************************************
    public static float getMutationProbability() {
        return mutationProbability;
    }
    public static float getCrossOverProbability() {
        return crossOverProbability;
    }
    public static Individu getId(int i){
        return pop.getIndividu(i);
    }
    public static byte getGene(int i,int j){
        return pop.getIndividu(i).getGene(j);
    }
//******************************************************************************
    //taille de la pop
    public static int getInitPopSize(){
        return initPopulationSize;
    }
    public static void updatePopSize(int whatMore){
        popSize += whatMore;
    }
    public static int getPopSize(){
        return popSize;
    }
//******************************************************************************
    //taille des chromosomes
    public static int getChromoSize(){
        return chromoSize;
    }
    public static void setChromoSize(int size){
        chromoSize = size;
    }
//******************************************************************************
    public static int[][] getMap(){
        return playGround.getGround();
    }
    public static boolean addToMap(int x, int y , byte s){       
        return ( playGround.addPeople(x,y,s) );
    }
    public static void addMark(int x, int y){   
        System.out.println("marquage");
        playGround.addMark(x, y);
    }
    public static int getmapW(){
        return mapWidth;
    }
    public static int getmapH(){
        return mapHeight;
    }
    public static int getNumberOfObstacles(){
        return playGround.getObstacles();
    }
    public static int getObstaclesX(int o){
        return playGround.getObsX(o);
    }
    public static int getObstaclesY(int o){
        return playGround.getObsY(o);
    }
//******************************************************************************
    public static byte getmessOffset(){
        return messageOffset;
    }
//******************************************************************************
    public static int getMaxAge(){
        return ageMaximum -128;
    }
//******************************************************************************
    public static byte getMaxChildren(){
        return maxChildren;
    }
//******************************************************************************
    public static void setAverageAge(float avAge) {
        averageAge = avAge;
    }
    public static float getAverageAge() {
        return averageAge;
    }
//*****************************
    public static void setDeathThisTurn(int deathThisTurn) {
        numberOfDeath = deathThisTurn;
    }
    public static int getDeathThisTurn() {
        return numberOfDeath;
    }
//*****************************
    public static void setRatioHF(float ratio) {
        ratioHF = ratio;
    }
    public static float getRatioHF() {
        return ratioHF;
    }
//*****************************
    public static void setReproThisTurn(int numberOfRepro) {
        numberOfReproductions = numberOfRepro;
    }
    public static int getReproThisTurn() {
        return numberOfReproductions;
    }
//*****************************
    public static void setThisTurnLoveMess(int loveMess) {
        loveMessages = loveMess;
    }
    public static int getThisTurnLoveMess() {
        return loveMessages;
    }
//*****************************
    public static void seTotDeath(int TotDeath) {
        TotalDeath = TotDeath;
    }
    public static int getTotDeath() {
        return TotalDeath;
    }
//*****************************
    public static void setTotChilds(int totRepro) {
        totalRepro = totRepro;
    }
    public static int getTotChilds() {
        return totalRepro;
    }
//******************************************************************************
    public void storeStatsRender() {
        lastTime = curTime;
        curTime = System.currentTimeMillis();
        totalTime += curTime - lastTime;
        if (totalTime > 1000) {
            totalTime -= 1000;
            ups = frames;
            frames = 0;
        }
        frames++;
    }
    
    public static int getUps(){
        return ups;
    }

}
