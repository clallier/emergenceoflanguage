
package algoGen;

import cern.colt.list.ByteArrayList;


/**
 *
 *Les individus
 * 
 * 1er gene stock le sexe de l'individu
 * (0/1)
 * 2e gene : stock age max
 * -128 à +127
 * 1ere partie ( n genes : stock les infos relatifs aux males )
 * 8 bytes : du 2 au 10 
 * 2eme partie ( m genes : -----------------------------femelles)
 * 24 bytes : du 11 au 35
 * Representation conditionnelle de l'information : si input a ( de 1 à n )
 * ... alors faire action A. Plusieurs n peuvent avoir le même A, 
 * étant donnée que l'algo doit trouver tout seul quelles actions doivent 
 * être utilisée avec quelles conditions.
 * Les femelles peuvent emettre un sons particulier selon la position du 
 * male par rapport à elle. Elle ne peuvent pas bouger
 * Les mâles doivent choisir une direction selon le son entendu 
 * Si un male et une femelle sont sur la même case, ils se reproduisent 
 * et le nouvel individu se place aléatoirement sur le playGround
 * 
 * les trucs pas génétiques
 * age
 * position x et y
 * fitness( depend du score total de la population )
 *
 */
public class Individu {
    private ByteArrayList genome;
    private float score = 0;
    private float fitness = 0;
    private int age = -128;
    private int posX;
    private int posY;
    private byte messType;


    public Individu(int genoSize) {
        
        // cree un individu aleatoire
        genome = new ByteArrayList(genoSize);
        //sexe
        genome.add(AG.getAByte(2,3));
        //age
        genome.add(AG.getAByte(-127,AG.getMaxAge()));
        //reste
        for(int i=2;i<genoSize+1;i++){
            genome.add(AG.getAByte(1,8));
            //System.out.println("gen"+i+" :"+genome.get(i) );
        }

        messType = 0;
        setNewPosition();
        //score = develop();
    }
//******************************************************************************
    public int getX(){
        return posX;
    }
    public int getY(){
        return posY;
    }
    //place aléatoirement l'individu sur la grille
    public void setNewPosition(){
         while( !(AG.addToMap( getX(), getY(), getSex())) ){
            posX = AG.getAByte(1, AG.getmapW()+1);
            posY = AG.getAByte(1, AG.getmapH()+1);
        }
    }
    public void move(int direction){
        int[][] map = AG.getMap();
//        System.out.println("en "+posX +" "+ posY);
        //AG.addMark(posX, posY );
        int N = map[posX][posY-1];
        int S = map[posX][posY+1];
        //AG.addMark(posX, posY+1 );
        int E = map[posX+1][posY];
        int W = map[posX-1][posY];
        switch (direction){
            case 1: if(N==1){posY--;} break;    //N
            case 5: if(S==1){posY++;} break;    //S
            case 3: if(E==1){posX++;} break;    //E
            case 7: if(W==1){posX--;} break;    //W
            case 2: if(N==1){posY--;}
                    if(E==1){posX++;} break;   //NE
            case 8: if(N==1){posY--;}
                    if(W==1){posX--;} break;   //NW
            case 4: if(S==1){posY++;}
                    if(E==1){posX++;} break;   //SE
            case 6: if(S==1){posY++;}
                    if(W==1){posX--;} break;   //SW
        default:
                break;
        }
        if(posX<1)
            posX = 1;
        if(posX>AG.getmapW()+1)
            posX = (byte)(AG.getmapW()+1);
        if(posY<1)
            posY = 1;
        if(posY>AG.getmapH()+1)
            posY = (byte)(AG.getmapH()+1);
    }
//******************************************************************************
    public byte getSex(){
        return getGene(0);
    }
//******************************************************************************
    public int getAge(){
        return age;
    }
    public byte getMaxAge(){
        return getGene(1);
    }
    public void updateAge(int i){
        age += i;
    }
//******************************************************************************
    //methodes set/get de fitness
    public void setFitness(float f){
        fitness = f;
    }
    public float getFitness(){
        return fitness;
    }
    public void upScore(byte u){
        score += u;
    }
    public float getScore(){
        return score;
    }
//******************************************************************************
    // Set/get de genes
    void setGene(int position, byte valeur){
        genome.set(position, valeur);
    }
    byte getGene(int position){
        return genome.get(position);
    }
    ByteArrayList getGenome(){
        return genome;
    }
//******************************************************************************
    // Set/get message 
    public void sendMessage(Individu cible, int messType){
        cible.messType = (byte)messType;
    }
    public byte getMessType(){
        return messType;
    }
 //*****************************************************************************
    // Applique une mutation : écrase un gene par une valeur aléatoire
    public byte mutation(int position)
    {
        
        byte value = genome.get(position);
        boolean mute = AG.isMuting();
        if( mute ) {
            int maxValue=8, minValue = 1;
            switch(position){
            // sexe
            case 0: minValue = 2; maxValue = 3;break;
            //age
            case 1: maxValue = AG.getMaxAge(); /*System.out.println("Nouveau age"+(maxValue+128) )*/ ;break;
            default:break;
            }
            value = AG.getAByte(minValue, maxValue);
        }
        return value; 
    }
    
}
