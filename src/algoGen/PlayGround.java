
package algoGen;

import cern.colt.list.ByteArrayList;

public class PlayGround {

    private int width,  height;
    private int[][] ground;
    private ByteArrayList obstaclesX;
    private ByteArrayList obstaclesY;
    private ByteArrayList markX;
    private ByteArrayList markY;

    public PlayGround(int mapWidth, int mapHeight, int type) {
        width = mapWidth;
        height = mapHeight;
//        System.out.println("w:" + width + "+2 h:" + height + "+2");
        ground = new int[(width + 2)][(height + 2)];
        obstaclesX = new ByteArrayList();
        obstaclesY = new ByteArrayList();
        markX = new ByteArrayList();
        markY = new ByteArrayList();
        // mise a zero/init  de la map
        for (int w = 1; w <= width; w++) {
            for (int h = 1; h <= height; h++) {
                //System.out.println("w(1->21):" + w + " h(1->11):" + h);
                ground[w][h] = 1;
                if (AG.getAFloat() < .03f) { //3% d'obstacles
                    ground[w][h] = 0;
                    obstaclesX.add((byte) w);
                    obstaclesY.add((byte) h);
                }
            }
        }
    }

    public int[][] getGround() {
        return ground;
    }
    //******************************************************************************

    public boolean addPeople(int x, int y, byte s) {
        //System.out.println("[x]"+x+"[y]"+y);
        try{
            int g = ground[x][y];
            if  ( g == 1) {
                ground[x][y] = s;
                return true;
            } else {
                return false;
            }
        }catch(ArrayIndexOutOfBoundsException exception){return false; }
    }

    public void addMark(int x, int y) {
        ground[x][y] = 4;
        markX.add((byte) x);
        markY.add((byte) y);
    }
    
    int getObsX(int o) {
       return obstaclesX.get(o);
    }

    int getObsY(int o) {
        return obstaclesY.get(o);
    }

    int getObstacles() {
        return obstaclesX.size();
    }
    //******************************************************************************
    // dessin du playGround

    public void print(Population pop) {
        //remise a zero
        for (int w = 1; w <= width; w++) {
            for (int h = 1; h <= height; h++) {
                ground[w][h] = 1;
            }
        }

        //rajout des marks
        for (int o = 0; o < markX.size(); o++) {
            byte y = markY.get(o);
            byte x = markX.get(o);
            ground[x][y] = 4;
        }
        //rajout des obstacles
        for (int o = 0; o < obstaclesX.size(); o++) {
            byte y = obstaclesY.get(o);
            byte x = obstaclesX.get(o);
            ground[x][y] = 0;
        }
        //rajout des gens
        for (int i = 0; i < AG.getPopSize(); i++) {
            int y = pop.getIndividu(i).getY();
            int x = pop.getIndividu(i).getX();
            byte s = pop.getIndividu(i).getSex();
            ground[x][y] = s;
        }
        //dessin
        //System.out.flush();
        for (int h = 0; h < height + 2; h++) {
            for (int w = 0; w < width + 2; w++) {
                switch (ground[w][h]) {
                    case 0:
                        System.out.print("0");
                        break;
                    case 1:
                        System.out.print(" ");
                        break;
                    case 2:
                        System.out.print("M");
                        break;
                    case 3:
                        System.out.print("F");
                        break;
                    case 4:
                        System.out.print("*");
                        break;
                    default:
                        ;
                }
            }
            System.out.println("");
        }
    }

}
